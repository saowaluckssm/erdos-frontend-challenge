const http = require("http");
const { faker } = require("@faker-js/faker");

function createRandomUser(days=3) {
    return {
      title: faker.address.city(),
      created: faker.date.recent(days),
      description: faker.lorem.paragraph()
    };
  }
let app = http.createServer((req, res) => {
  res.writeHead(200, { "Content-Type": "text/json", "Access-Control-Allow-Origin": "*"});

  // Send back a response and end the connection
  res.end(JSON.stringify(Array.from({ length: 10 }).map((value, index) => createRandomUser(index))));
});

app.listen(3003, "127.0.0.1");
console.log("Node server running on port 3003");
