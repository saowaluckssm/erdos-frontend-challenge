import { useAppDispatch, useAppSelector } from '../hooks';
import styles from '../styles/Home.module.css';
import { useEffect } from 'react';
import { fetchPosts } from '../slices/postSlice';
import { selectOldPosts, selectRecentPosts } from '../slices/postSelector';
import { CardItem, NavBar } from './components';

export default function Home() {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(fetchPosts());
  }, [dispatch]);
  const recentPosts = useAppSelector(selectRecentPosts);
  const oldPosts = useAppSelector(selectOldPosts);
  const recentPostsSortByDate = recentPosts.sort((a, b) => {
    return new Date(a.created).getTime() - new Date(b.created).getTime();
  });

  const oldPostsSortByDate = oldPosts.sort((a, b) => {
    return new Date(a.created).getTime() - new Date(b.created).getTime();
  });

  return (
    <div className={styles.mainContainer}>
      <NavBar />
      <div className={styles.container}>
        <h1 className={styles.mainHeader}>Recent Posts</h1>
        <hr className={styles.divider} />
        <div className={styles.cardContainer}>
          {recentPostsSortByDate.map((post, index) => (
            <CardItem
              key={`recent-post${index}`}
              post={{
                title: post.title,
                description: post.description,
                created: post.created,
              }}
            />
          ))}
        </div>

        <h1 className={styles.mainHeader}>Old Posts</h1>
        <hr className={styles.divider} />
        <div className={styles.cardContainer}>
          {oldPostsSortByDate.map((post, index) => (
            <CardItem
              key={`old-post${index}`}
              post={{
                title: post.title,
                description: post.description,
                created: post.created,
              }}
            />
          ))}
        </div>
      </div>
    </div>
  );
}
