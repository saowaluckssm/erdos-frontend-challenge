const NavIcon = () => (
  <svg
    width="35"
    height="30"
    viewBox="0 0 35 30"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <line y1="4.5" x2="35" y2="4.5" stroke="#828282" />
    <line y1="14" x2="35" y2="14" stroke="#828282" strokeWidth="2" />
    <line y1="23.5" x2="35" y2="23.5" stroke="#828282" strokeWidth="3" />
  </svg>
);

export default NavIcon;
