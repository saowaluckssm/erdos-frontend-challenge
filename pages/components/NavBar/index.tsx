import styles from '../../../styles/Home.module.css';
import NavIcon from '../icons/svgs/NavIcon';

export type NavBarProps = {};

export const NavBar = () => {
  return (
    <div className={styles.navBar}>
      <div className={styles.navIcon}>
        <NavIcon />
      </div>
    </div>
  );
};
