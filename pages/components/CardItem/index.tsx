import styles from '../../../styles/Home.module.css';
import Image from 'next/image';
import { useEffect } from 'react';
import Aos from 'aos';
import 'aos/dist/aos.css';

export type CardItemProps = {
  post: {
    title: string;
    created: Date;
    description: string;
  };
};

export const CardItem = ({ post }: CardItemProps) => {
  const date = new Date(post.created).toDateString();

  useEffect(() => {
    Aos.init({ duration: 2000 });
  }, []);

  return (
    <div className={styles.hoverWrapper}>
      <div data-aos="fade-left" className={styles.cardBody}>
        <div className={styles.imageContainer}>
          <Image
            src="/IMG_0684.PNG"
            alt="image"
            width={180}
            height={180}
            className={styles.cardImage}
          />
        </div>
        <div className={styles.contentWrapper}>
          <h2 className="cardTitle">{post.title}</h2>
          <p>{post.description}</p>
          <p className={styles.createAt}>Create at {date}</p>
        </div>
      </div>
    </div>
  );
};
