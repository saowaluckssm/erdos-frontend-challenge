# React Challenge

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

It has couple dependacies already added including:
- Typescript
- Redux Tool Kit
- [Material UI](https://mui.com/material-ui/getting-started/overview/)
- [Ant Design](https://ant.design/components/overview/)

### Redux
A redux store has already been initialized and setup for you with devtools. There is a `PostSlice` that stores posts. The initial page fetches all the posts using an async thunk.

## Getting Started

First, run the api server:

```bash
node server.js
```

Then run the Next.js app:
```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.tsx`. The page auto-updates as you edit the file.

## Technical Challenge

### Part 1: Recent Posts
Update the Post Selectors (in `slices/postSelector.ts`) to filter recent posts and old posts. Recent posts are posts that were create today. Old posts are all posts that created more than a day ago, (they include yesterday and beyond).

### Part 2: React Design
Using the design provided here, update the index page (in `pages/index.ts`) accordingly.

### Part 3: Animation
Add some animation to where you would like.