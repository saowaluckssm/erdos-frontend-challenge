import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store';

export const selectPosts = createSelector(
  (state: RootState) => state.post.posts,
  (posts) => posts
);

export const selectRecentPosts = createSelector(
  (state: RootState) => state.post.posts,
  (posts) =>
    posts.filter((post) => {
      const date = new Date(post.created);
      const yesterday = new Date(Date.now() - 86400000);
      return date >= yesterday;
    })
);

export const selectOldPosts = createSelector(
  (state: RootState) => state.post.posts,
  (posts) =>
    posts.filter((post) => {
      const date = new Date(post.created);
      const yesterday = new Date(Date.now() - 86400000);
      return date < yesterday;
    })
);
