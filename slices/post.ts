export interface Post {
  title: string;
  created: Date;
  description: string;
}
